export default async () => {
  const payload = { email: "test@test.com", password: "123" };
  const url = "https://reqres.in/api/login";
  try {
    const data = await fetch(url, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: { "Content-Type": "application/json" }
    });
    return data.json();
  } catch (e) {
    return null;
  }
};
