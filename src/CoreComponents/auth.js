export const TOKEN = "@token";
export const LOGADO = "@logado";

const auth = {
  login(token) {
    localStorage.setItem(TOKEN, token);
    localStorage.setItem(LOGADO, true);
  },
  logout() {
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(LOGADO);
  },
  isAuthenticated() {
    return localStorage.getItem(LOGADO) || false;
  }
};

export default auth;
